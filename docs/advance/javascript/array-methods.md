# 通过实现25个数组方法来理解及高效使用数组方法(长文,建议收藏)

## forEach

<font color=#FFF5DF5>Array.prototype.forEach </font>方法对数组的每个元素执行一次提供的函数，而且不会改变原数组。

```JS
[1, 2, 3, 4, 5].forEach(value => console.log(value));
```

**实现**

```js
function forEach(array, callback) {
  const { length } = array;
  
  for (let index = 0; index < length; index += 1) {
    const value = array[index];
    callback(value, index, array)
  }
}
```

咱们遍历数组并为每个元素执行回调。这里需要注意的一点是，该方法没有返回什么，所以默认返回<font color=#FFF5DF5>undefined</font>。

**方法涟**

使用数组方法的好处是可以将操作链接在一起。考虑以下代码：

```js
function getTodosWithCategory(todos, category) {
 return todos
   .filter(todo => todo.category === category)
   .map(todo => normalizeTodo(todo));
}
```

这种方式，咱们就不必将<font color=#FFF5DF5>map</font>的执行结果保存到变量中，代码会更简洁。

不幸的是，<font color=#FFF5DF5>forEach</font>没有返回原数组，这意味着咱们不能做下面的事情

```js
// 无法工作
function getTodosWithCategory(todos, category) {
 return todos
   .filter(todo => todo.category === category)
   .forEach((value) => console.log(value))
   .map(todo => normalizeTodo(todo));
}
```

### 帮助函数 （打印信息）

接着实现一个简单的函数，它能更好地解释每个方法的功能:接受什么作为输入，返回什么，以及它是否对数组进行了修改。

```js
function logOperation(operationName, array, callback) {
 const input = [...array];
 const result = callback(array);

 console.log({
   operation: operationName,
   arrayBefore: input,
   arrayAfter: array,
   mutates: mutatesArray(input, array), // shallow check
   result,
 });
}
```

其中 mutatesArray 方法用来判断是否更改了原数组，如果有修改刚返回 <font color=#FFF5DF5>true</font>，否则返回<font color=#FFF5DF5>false</font> 。当然大伙有好的想法可以在评论提出呦。

```js
function mutatesArray(firstArray, secondArray) {
  if (firstArray.length !== secondArray.length) {
    return true;
  }

  for (let index = 0; index < firstArray.length; index += 1) {
    if (firstArray[index] !== secondArray[index]) {
      return true;
    }
  }

  return false;
}
```

然后使用<font color=#FFF5DF5>logOperation</font>来测试咱们前面自己实现的 <font color=#FFF5DF5>forEach</font>方法。

```js
logOperation('forEach', [1, 2, 3, 4, 5], array => forEach(array, value => console.log(value)));
```

打印结果：

```js
{
  operation: 'forEach',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: undefined
}
```

-----------------------

## map

<font color=#FFF5DF5>map</font> 方法会给原数组中的每个元素都按顺序调用一次 <font color=#FFF5DF5>callback</font> 函数。<font color=#FFF5DF5>callback</font> 每次执行后的返回值（包括 <font color=#FFF5DF5>undefined</font>）组合起来形成一个新数组。

**实现**

```js
function map(array, callback) {
  const result = [];
  const { length } = array;
  
  for (let index = 0; index < length; index +=1) {
    const value = array[index];
    
    result[index] = callback(value, index, array);
  }

  return result;
}
```

提供给方法的回调函数接受旧值作为参数，并返回一个新值，然后将其保存在新数组中的相同索引下，这里用变量 <font color=#FFF5DF5>result </font> 表示。

这里需要注意的是，咱们返回了一个新的数组，不修改旧的。

**测试**

```js
logOperation('map', [1, 2, 3, 4, 5], array => map(array, value => value + 5));
```

打印结果：

```js
{ 
  operation: 'map',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: [ 6, 7, 8, 9, 10 ]
 }
```

--------------------

## filter

<font color=#FFF5DF5>Array.prototype.filter </font> 过滤回调返回为<font color=#FFF5DF5>false </font>的值，每个值都保存在一个新的数组中，然后返回。

```js
[1, 2, 3, 4, 5].filter(number => number >= 3);
// -> [3, 4, 5]
```

**实现**

```js
function push(array, ...values) {
  const { length: arrayLength } = array;
  const { length: valuesLength } = values;

  for (let index = 0; index < valuesLength; index += 1) {
    array[arrayLength + index] = values[index];
  }

  return array.length;
}
--------------------------------------------------
function filter(array, callback) {
 const result = [];

 const { length } = array;

 for (let index = 0; index < length; index += 1) {
   const value = array[index];

   if (callback(value, index, array)) {
     push(result, value);
   }
 }

 return result;
}
```

获取每个值并检查所提供的回调函数是否返回<font color=#FFF5DF5>true </font>或<font color=#FFF5DF5>false </font>，然后将该值添加到新创建的数组中，或者适当地丢弃它。

注意，这里对<font color=#FFF5DF5>result </font> 数组使用<font color=#FFF5DF5>push </font>方法，而不是将值保存在传入数组中放置的相同索引中。这样，<font color=#FFF5DF5>result </font>就不会因为丢弃的值而有空槽。

**测试**

```js
logOperation('filter', [1, 2, 3, 4, 5], array => filter(array, value => value >= 2));
```

运行：

```js
{ 
  operation: 'filter',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: [ 2, 3, 4, 5 ] 
}
```

---------------------

## reduce

<font color=#FFF5DF5>reduce() </font> 方法接收一个函数作为累加器，数组中的每个值（从左到右）开始缩减，最终计算为一个值。<font color=#FFF5DF5>reduce() </font> 方法接受四个参数：**初始值（或者上一次回调函数的返回值），当前元素值，当前索引，调用 reduce() 的数组。**

确切地说，如何计算该值是需要在回调中指定的。来看呓使用<font color=#FFF5DF5>reduce </font>的一个简单的例子：对一组数字求和：

```js
 [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].reduce((sum, number) => {
   return sum + number;
 }, 0) // -> 55
```

注意这里的回调接受两个参数:<font color=#FFF5DF5>sum </font>和<font color=#FFF5DF5>number </font>。第一个参数总是前一个迭代返回的结果，第二个参数在遍历中的当前数组元素。

这里，当咱们对数组进行迭代时，<font color=#FFF5DF5>sum </font>包含到循环当前索引的所有数字的和因为每次迭代咱们都将数组的当前值添加到<font color=#FFF5DF5>sum </font>中。

**实现**

```js
function reduce(array, callback, initValue) {
  const { length } = array;
  
  let acc = initValue;
  let startAtIndex = 0;

  if (initValue === undefined) {
    acc = array[0];
    startAtIndex = 0;
  }

  for (let index = startAtIndex; index < length; index += 1) {
    const value = array[index];
    acc = callback(acc, value, index, array)
  }
 
  return acc;
}
```

咱们创建了两个变量<font color=#FFF5DF5>acc </font>和<font color=#FFF5DF5>startAtIndex </font>，并用它们的默认值初始化它们，分别是参数<font color=#FFF5DF5>initValue和 </font><font color=#FFF5DF5>0 </font>。

**测试**

```js
logOperation('reduce', [1, 2, 3, 4, 5], array => reduce(array, (sum, number) => sum + number, 0));
```

**运行：**

```js
{ operation: 'reduce',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: 15 
}
```

--------------------

## findIndex

<font color=#FFF5DF5>findIndex </font>帮助咱们找到数组中给定值的索引。

```js
[1, 2, 3, 4, 5, 6, 7].findIndex(value => value === 5); // 4
```

<font color=#FFF5DF5>findIndex </font>方法对数组中的每个数组索引<font color=#FFF5DF5>0..length-1 </font>（包括）执行一次<font color=#FFF5DF5>callback </font>函数，直到找到一个<font color=#FFF5DF5>callback </font>函数返回真实值（强制为<font color=#FFF5DF5> true </font>）的值。如果找到这样的元素，<font color=#FFF5DF5>findIndex </font>会立即返回该元素的索引。如果回调从不返回真值，或者数组的<font color=#FFF5DF5> length </font>为<font color=#FFF5DF5> 0 </font>，则<font color=#FFF5DF5>findIndex </font>返回<font color=#FFF5DF5> -1 </font>。

**实现**

```js
function findIndex(array, callback) {
 const { length } = array;

 for (let index = 0; index < length; index += 1) {
   const value = array[index];

   if (callback(value, index, array)) {
     return index;
   }
 }

 return -1;
}
```

**测试**

```js
logOperation('findIndex', [1, 2, 3, 4, 5], array => findIndex(array, number => number === 3));
```

**运行：**

```js
{
  operation: 'findIndex',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: 2
}
```

---------------------

## find

<font color=#FFF5DF5>find </font>与<font color=#FFF5DF5>findIndex </font>的唯一区别在于，它返回的是实际值，而不是索引。实际工作中，咱们可以重用已经实现的<font color=#FFF5DF5>findIndex </font>。

```js
[1, 2, 3, 4, 5, 6, 7].find(value => value === 5); // 5
```

**实现**

```js
function find(array, callback) {
 const index = findIndex(array, callback);

 if (index === -1) {
   return undefined;
 }

 return array[index];
}
```

**测试**

```js
logOperation('find', [1, 2, 3, 4, 5], array => find(array, number => number === 3));
```

**运行**

```js
{
  operation: 'find',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: 3
}
```

----------------------

## indexOf

<font color=#FFF5DF5>indexOf </font>是获取给定值索引的另一种方法。然而，这一次，咱们将实际值作为参数而不是函数传递。同样，为了简化实现，可以使用前面实现的<font color=#FFF5DF5>indexOf </font>

```js
[3, 2, 3].indexOf(3); // -> 0
```

**实现**

```js
function indexOf(array, searchedValue) {
  return findIndex(array, value => value === searchedValue)
}
```

**测试**

```js
logOperation('indexOf', [1, 2, 3, 4, 5], array => indexOf(array, 3));
```

**运行**

```js
{
  operation: 'indexOf',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: 2
}
```

----------------------

## lastIndexOf

lastIndexOf的工作方式与<font color=#FFF5DF5>indexOf </font>相同，<font color=#FFF5DF5>lastIndexOf() </font> 方法返回指定元素在数组中的最后一个的索引，如果不存在则返回 -1。

```js
[3, 2, 3].lastIndexOf(3); // -> 2
```

**实现**

```js
function lastIndexOf(array, searchedValue) {
  for (let index = array.length - 1; index > -1; index -= 1 ){
    const value = array[index];
    
    if (value === searchedValue) {
      return index;
    }
  }
  return  -1;
}
```

代码基本与<font color=#FFF5DF5>findIndex </font>类似，但是没有执行回调，而是比较<font color=#FFF5DF5>value </font>和<font color=#FFF5DF5>searchedValue </font>。如果比较结果为 <font color=#FFF5DF5>true </font>，则返回索引,如果找不到值，返回<font color=#FFF5DF5>-1 </font>。

**测试**

```js
logOperation('lastIndexOf', [1, 2, 3, 4, 5, 3], array => lastIndexOf(array, 3));
```

**运行**

```js
{ 
  operation: 'lastIndexOf',
  arrayBefore: [ 1, 2, 3, 4, 5, 3 ],
  arrayAfter: [ 1, 2, 3, 4, 5, 3 ],
  mutates: false,
  result: 5 
}
```

----------------------

## every

<font color=#FFF5DF5>every() </font> 方法测试一个数组内的所有元素是否都能通过某个指定函数的测试，它返回一个布尔值。

```js
[1, 2, 3].every(value => Number.isInteger(value)); // -> true
```

咱们可以将<font color=#FFF5DF5>every </font> 方法看作一个等价于逻辑与的数组。

**实现**

```js
function every(array, callback){
  const { length } = array;
  
  for (let index = 0; index < length; index += 1) {
   const value = array[index];
   
    if (!callback(value, index, array)) {
      return false;
    }
  }

  return true;
}
```

咱们为每个值执行回调。如果在任何时候返回<font color=#FFF5DF5>false </font>，则退出循环，整个方法返回<font color=#FFF5DF5>false </font>。如果循环终止而没有进入到<font color=#FFF5DF5>if </font>语句里面(说明条件都成立)，则方法返回<font color=#FFF5DF5>true </font>。

**测试**

```js
logOperation('every', [1, 2, 3, 4, 5], array => every(array, number => Number.isInteger(number)));
```

**运行**

```js
{
  operation: 'every',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: true 
}
```

-----------------

## some

<font color=#FFF5DF5>some </font> 方法与 <font color=#FFF5DF5>every </font> 刚好相反，即只要其中一个为<font color=#FFF5DF5>true </font> 就会返回<font color=#FFF5DF5>true </font>。与<font color=#FFF5DF5>every </font> 方法类似，咱们可以将<font color=#FFF5DF5>some </font> 方法看作一个等价于逻辑或数组。

```js
[1, 2, 3, 4, 5].some(number => number === 5); // -> true
```

**实现**

```js
function some(array, callback) {
 const { length } = array;

 for (let index = 0; index < length; index += 1) {
   const value = array[index];

   if (callback(value, index, array)) {
     return true;
   }
 }

 return false;
}
```

咱们为每个值执行回调。如果在任何时候返回<font color=#FFF5DF5>true </font>，则退出循环，整个方法返回<font color=#FFF5DF5>true </font>。如果循环终止而没有进入到<font color=#FFF5DF5>if </font>语句里面(说明条件都成立)，则方法返回<font color=#FFF5DF5>false </font>。

**测试**

```js
logOperation('some', [1, 2, 3, 4, 5], array => some(array, number => number === 5));
```

**运行**

```js
{
  operation: 'some',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: true
}
```

------------------------

## includes

<font color=#FFF5DF5>includes </font>方法的工作方式类似于 <font color=#FFF5DF5>some </font> 方法，但是<font color=#FFF5DF5>includes </font>不用回调，而是提供一个参数值来比较元素。

```js
[1, 2, 3].includes(3); // -> true
```

**实现**

```js
function includes(array, searchedValue){
  return some(array, value => value === searchedValue)
}
```

**测试**

```js
logOperation('includes', [1, 2, 3, 4, 5], array => includes(array, 5));
```

**运行**

```js
{
  operation: 'includes',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: true
}
```

------------------

## concat

<font color=#FFF5DF5>concat() </font>方法用于合并两个或多个数组，此方法不会更改现有数组，而是返回一个新数组。

```js
[1, 2, 3].concat([4, 5], 6, [7, 8]) // -> [1, 2, 3, 4, 5, 6, 7, 8]
```

**实现**

```js
function concat(array, ...values) {
  const result = [...array];
  const { length } = values;

  for (let index = 0; index < length; index += 1) {
    const value = values[index];
    
    if (Array.isArray(value)) {
      push(result, ...value);
    } else {
      push(result, value);
    }
  }

  return result;
}
```

<font color=#FFF5DF5>concat </font>将数组作为第一个参数，并将未指定个数的值作为第二个参数，这些值可以是数组，也可以是其他类型的值。

首先，通过复制传入的数组创建 <font color=#FFF5DF5>result </font> 数组。然后，遍历 <font color=#FFF5DF5>values  </font> ，检查该值是否是数组。如果是，则使用<font color=#FFF5DF5>push </font>函数将其值附加到结果数组中。

<font color=#FFF5DF5>push(result, value) </font> 只会向数组追加为一个元素。相反，通过使用展开操作符<font color=#FFF5DF5>push(result，…value) </font> 将数组的所有值附加到result<font color=#FFF5DF5>result </font> 数组中。在某种程度上，咱们把数组扁平了一层。



**测试**

```js
logOperation('concat', [1, 2, 3, 4, 5], array => concat(array, 1, 2, [3, 4]));
```

**运行**

```js
{ 
 operation: 'concat',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: [ 1, 2, 3, 4, 5, 1, 2, 3, 4 ] 
}
```

--------------------

## join

<font color=#FFF5DF5>join() </font>方法用于把数组中的所有元素放入一个字符串，元素是通过指定的分隔符进行分隔的。

```js
['Brian', 'Matt', 'Kate'].join(', ') // -> Brian, Matt, Kate
```

**实现**

```js
function join(array, joinWith) {
  return reduce(
    array,
    (result, current, index) => {
      if (index === 0) {
        return current;
      }
      
      return `${result}${joinWith}${current}`;
    },
    ''
  )
}
```

<font color=#FFF5DF5>reduce </font>的回调是神奇之处:<font color=#FFF5DF5>reduce </font>遍历所提供的数组并将结果字符串拼接在一起，在数组的值之间放置所需的分隔符(作为<font color=#FFF5DF5>concat </font>joinWith传递)。

<font color=#FFF5DF5>array[0] </font>值需要一些特殊的处理，因为此时<font color=#FFF5DF5>result </font>是一个空字符串，而且咱们也不希望分隔符<font color=#FFF5DF5>(joinWith) </font>位于第一个元素前面。


**测试**

```js
logOperation('join', [1, 2, 3, 4, 5], array => join(array, ', '));
```

**运行**

```js
{
  operation: 'join',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: '1, 2, 3, 4, 5'
}
```

------------------

## reverse

<font color=#FFF5DF5>reverse() </font>方法将数组中元素的位置颠倒，并返回该数组，该方法会改变原数组。

**实现**

```js
function reverse(array) {
  const result = []
  const lastIndex = array.length - 1;

  for (let index = lastIndex; index > -1; index -= 1) {
    const value = array[index];
    result[lastIndex - index ] = value
  }
  return result;
}
```

其思路很简单:首先，定义一个空数组，并将数组的最后一个索引保存<font color=#FFF5DF5>为变量(lastIndex) </font>。接着反过来遍历数组，将每个值保存在结果<font color=#FFF5DF5>result </font> 中的<font color=#FFF5DF5>(lastIndex - index) </font>位置，然后返回<font color=#FFF5DF5>result </font>数组。



**测试**

```js
logOperation('reverse', [1, 2, 3, 4, 5], array => reverse(array));
```

**运行**

```js
{
  operation: 'reverse',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: [ 5, 4, 3, 2, 1 ]
}
```

---------------------

## join

<font color=#FFF5DF5>join() </font>方法用于把数组中的所有元素放入一个字符串，元素是通过指定的分隔符进行分隔的。

```js
['Brian', 'Matt', 'Kate'].join(', ') // -> Brian, Matt, Kate
```

**实现**

```js
function join(array, joinWith) {
  return reduce(
    array,
    (result, current, index) => {
      if (index === 0) {
        return current;
      }
      
      return `${result}${joinWith}${current}`;
    },
    ''
  )
}
```

<font color=#FFF5DF5>reduce </font>的回调是神奇之处:<font color=#FFF5DF5>reduce </font>遍历所提供的数组并将结果字符串拼接在一起，在数组的值之间放置所需的分隔符(作为<font color=#FFF5DF5>concat </font>joinWith传递)。

<font color=#FFF5DF5>array[0] </font>值需要一些特殊的处理，因为此时<font color=#FFF5DF5>result </font>是一个空字符串，而且咱们也不希望分隔符<font color=#FFF5DF5>(joinWith) </font>位于第一个元素前面。


**测试**

```js
logOperation('join', [1, 2, 3, 4, 5], array => join(array, ', '));
```

**运行**

```js
{
  operation: 'join',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: '1, 2, 3, 4, 5'
}
```

------------------

## shift

<font color=#FFF5DF5>shift() </font>方法从数组中删除第一个元素，并返回该元素的值，此方法更改数组的长度。

```js
[1, 2, 3].shift(); // -> 1
```

**实现**

```js
function shift(array) {
  const { length } = array;
  const firstValue = array[0];

  for (let index = 1; index > length; index += 1) {
    const value = array[index];
    array[index - 1] = value;
  }

  array.length = length - 1;

  return firstValue;
}
```

首先保存数组的原始长度及其初始值，然后遍历数组并将每个值向下移动一个索引。完成遍历后，更新数组的长度并返回初始值。

**测试**

```js
logOperation('shift', [1, 2, 3, 4, 5], array => shift(array));
```

**运行**

```js
{
  operation: 'shift',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 2, 3, 4, 5 ],
  mutates: true,
  result: 1
}
```

------------------

## unshift

<font color=#FFF5DF5>unshift() </font>方法将一个或多个元素添加到数组的开头，并返回该数组的新长度(该方法修改原有数组)。


```js
[2, 3, 4].unshift(1); // -> [1, 2, 3, 4]
```

**实现**

```js
function unshift(array, ...values) {
  const mergedArrays = concat(values, ...array);
  const { length: mergedArraysLength } = mergedArrays;

  for (let index = 0; index < mergedArraysLength; index += 1) {
    const value = mergedArrays[index];
    array[index] = value;
  }

  return array.length;
}
```

首先将需要加入数组**值**(作为参数传递的单个值)和**数组**拼接起来。这里需要注意的是，<font color=#FFF5DF5>values </font> 放在第一位的，也就是放置在原始数组的前面。

然后保存这个新数组的长度并遍历它，将它的值保存在原始数组中，并覆盖开始时的值。

**测试**

```js
logOperation('unshift', [1, 2, 3, 4, 5], array => unshift(array, 0));
```

**运行**

```js
{
  operation: 'unshift',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 0, 1, 2, 3, 4, 5 ],
  mutates: true,
  result: 6
}
```

---------------

## slice

<font color=#FFF5DF5>slice() </font>方法返回一个新的数组对象，这一对象是一个由 begin 和 end 决定的原数组的浅拷贝（包括 begin，不包括end）原始数组不会被改变。

<font color=#FFF5DF5>slice() </font> 会提取原数组中索引从 begin 到 end 的所有元素（包含 begin，但不包含 end）。

```js
[1, 2, 3, 4, 5, 6, 7].slice(3, 6); // -> [4, 5, 6]
```

**实现**

```js
function slice(array, startIndex = 0, endIndex = array.length) {
 const result = [];

 for (let index = startIndex; index < endIndex; index += 1) {
   const value = array[index];

   if (index < array.length) {
     push(result, value);
   }
 }

 return result;
}
```

咱们遍历数组从startIndex到endIndex，并将每个值放入result。这里使用了这里的默认参数，这样当没有传递参数时，slice方法只创建数组的副本。

注意:if语句确保只在原始数组中存在给定索引下的值时才加入 result 中。

**测试**

```js
logOperation('slice', [1, 2, 3, 4, 5], array => slice(array, 1, 3));
```

**运行**

```js
{
  operation: 'slice',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4, 5 ],
  mutates: false,
  result: [ 2, 3 ]
}
```

-------------------------

## splice

<font color=#FFF5DF5>splice() </font>方法通过删除或替换现有元素或者原地添加新的元素来修改数组,并以数组形式返回被修改的内容。此方法会改变原数组。

首先，指定起始索引，然后指定要删除多少个值，其余的参数是要插入的值。

```js
const arr = [1, 2, 3, 4, 5];
// 从位置0开始，删除2个元素后插入 3, 4, 5
arr.splice(0, 2, 3, 4, 5);

arr // -> [3, 4, 5, 3, 4, 5]
```

**实现**

```js
function splice( array, insertAtIndex, removeNumberOfElements, ...values) {
  const firstPart = slice(array, 0, insertAtIndex);
  const secondPart = slice(array, insertAtIndex + removeNumberOfElements);

  const removedElements = slice(
    array,
    insertAtIndex,
    insertAtIndex + removeNumberOfElements
  );

  const joinedParts = firstPart.concat(values, secondPart);
  const { length: joinedPartsLength } = joinedParts;

  for (let index = 0; index < joinedPartsLength; index += 1) {
    array[index] = joinedParts[index];
  }

  array.length = joinedPartsLength;

  return removedElements;
}
```

其思路是在insertAtIndex和insertAtIndex + removeNumberOfElements上进行两次切割。这样，将原始数组切成三段。第一部分(firstPart)和第三部分(secondPart)加个插入的元素组成为最后数组的内容。


**测试**

```js
logOperation('splice', [1, 2, 3, 4, 5], array => splice(array, 1, 3));
```

**运行**

```js
{
  operation: 'splice',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 5 ],
  mutates: true,
  result: [ 2, 3, 4 ]
}
```

-----------------

## pop

<font color=#FFF5DF5>pop() </font>方法从数组中删除最后一个元素，并返回该元素的值。此方法更改数组的长度。

**实现**

```js
function pop(array) {
 const value = array[array.length - 1];

 array.length = array.length - 1;

 return value;
}
```

首先，将数组的最后一个值保存在一个变量中。然后只需将数组的长度减少1，从而删除最后一个值。


**测试**

```js
logOperation('pop', [1, 2, 3, 4, 5], array => pop(array));
```

**运行**

```js
{
  operation: 'pop',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [ 1, 2, 3, 4 ],
  mutates: true,
  result: 5
}
```

---------------------

## push

<font color=#FFF5DF5>push() </font>方法将一个或多个元素添加到数组的末尾，并返回该数组的新长度。

```js
[1, 2, 3, 4].push(5); // -> [1, 2, 3, 4, 5]
```

**实现**

```js
function push(array, ...values) {
  const { length: arrayLength } = array;
  const { length: valuesLength } = values;

  for (let index = 0; index < valuesLength; index += 1) {
    array[arrayLength + index] = values[index];
  }

  return array.length;
}
```

首先，我们保存原始数组的长度，以及在它们各自的变量中要添加的值。然后，遍历提供的值并将它们添加到原始数组中。

**测试**

```js
logOperation('push', [1, 2, 3, 4, 5], array => push(array, 6, 7));
```

**运行**

```js
{
  operation: 'push',
  arrayBefore: [ 1, 2, 3, 4, 5 ],
  arrayAfter: [
    1, 2, 3, 4,5, 6, 7
  ],
  mutates: true,
  result: 7
}
```

---------------

## fill

当咱们想用一个占位符值填充一个空数组时，可以使用fill方法。如果想创建一个指定数量的null元素数组，可以这样做:

<font color=#FFF5DF5>fill </font>方法真正做的是替换指定索引范围内的数组的值。如果没有提供范围，该方法将替换所有数组的值。

```js
[...Array(5)].fill(null) // -> [null, null, null, null, null]
```

**实现**

```js
function fill(array, value, startIndex = 0, endIndex = array.length) {
 for (let index = startIndex; index < endIndex; index += 1) {
   array[index] = value;
 }

 return array;
}
```

**测试**

```js
logOperation("fill", [...new Array(5)], array => fill(array, 0));
```

**运行**

```js
{
  operation: 'fill',
  arrayBefore: [ undefined, undefined, undefined, undefined, undefined ],
  arrayAfter: [ 0, 0, 0, 0, 0 ],
  mutates: true,
  result: [ 0, 0, 0, 0, 0 ]
}
```

--------------------

## flat

<font color=#FFF5DF5>flat </font>方法通过可指定深度值来减少嵌套的深度。

```js
[1, 2, 3, [4, 5, [6, 7, [8]]]].flat(1); // -> [1, 2, 3, 4, 5, [6, 7, [8]]]
```

因为展开的深度值是1，所以只有第一级数组是被扁平，其余的保持不变

```js
[1, 2, 3, [4, 5]].flat(1) // -> [1, 2, 3, 4, 5]
```

**实现**

```js
function flat(array, depth = 0) {
 if (depth < 1 || !Array.isArray(array)) {
   return array;
 }

 return reduce(
   array,
   (result, current) => {
     return concat(result, flat(current, depth - 1));
   },
   [],
 );
}
```

首先，我们检查depth参数是否小于1。如果是，那就意味着没有什么要扁平的，咱们应该简单地返回数组。
其次，咱们检查数组参数是否属于数组类型，因为如果它不是，那么扁化就没有意义了，所以只返回这个参数。
咱们们使用了之前实现的reduce函数。从一个空数组开始，然后取数组的每个值并将其扁平。
注意，我们调用带有(depth - 1)的flat函数。每次调用时，都递减depth参数，以免造成无限循环。扁平化完成后，将返回值来回加到result数组中。

**测试**

```js
logOperation('flat', [1, 2, 3, [4, 5, [6]]], array => flat(array, 2));
```

**运行**

```js
{
  operation: 'flat',
  arrayBefore: [ 1, 2, 3, [ 4, 5, [Array] ] ],
  arrayAfter: [ 1, 2, 3, [ 4, 5, [Array] ] ],
  mutates: false,
  result: [ 1, 2, 3, 4, 5, 6 ]
}
```

---------------------

## flatMap

<font color=#FFF5DF5>flatMap </font>方法首先使用映射函数映射每个元素，然后将结果压缩成一个新数组。它与 map 和 深度值1的 flat 几乎相同，但 flatMap 通常在合并成一种方法的效率稍微高一些。
在上面的map方法中，对于每个值，只返回一个值。这样，一个包含三个元素的数组在映射之后仍然有三个元素。使用flatMap，在提供的回调函数中，可以返回一个数组，这个数组稍后将被扁平。

```js
[1, 2, 3].flatMap(value => [value, value, value]); // [1, 1, 1, 2, 2, 2, 3, 3, 3]
```

每个返回的数组都是扁平的，我们得到的不是一个嵌套了三个数组的数组，而是一个包含9个元素的数组。

**实现**

```js
function flatMap(array, callback) {
 return flat(map(array, callback), 1);
}
```

首先使用map，然后将数组的结果数组扁平化一层。

**测试**

```js
logOperation('flatMap', [1, 2, 3], array => flatMap(array, number => [number, number]));
```

**运行**

```js
{
  operation: 'flatMap',
  arrayBefore: [ 1, 2, 3 ],
  arrayAfter: [ 1, 2, 3 ],
  mutates: false,
  result: [ 1, 1, 2, 2, 3, 3 ]
}
```

--------------------

## values

<font color=#FFF5DF5>values </font>方法返回一个生成器，该生成器生成数组的值。

```js
const valuesGenerator = values([1, 2, 3, 4, 5]);

valuesGenerator.next(); // { value: 1, done: false }
```

**实现**

```js
function values(array) {
 const { length } = array;

 function* createGenerator() {
   for (let index = 0; index < length; index += 1) {
     const value = array[index];
     yield value;
   }
 }

 return createGenerator();
}
```

首先，咱们定义createGenerator函数。在其中，咱们遍历数组并生成每个值。

----------------------

## keys

<font color=#FFF5DF5>keys </font>方法返回一个生成器，该生成器生成数组的索引。

```js
const keysGenerator = keys([1, 2, 3, 4, 5]);

keysGenerator.next(); // { value: 0, done: false }
```

**实现**

```js
function keys(array) {
 function* createGenerator() {
   const { length } = array;

   for (let index = 0; index < length; index += 1) {
     yield index;
   }
 }

 return createGenerator();
}
```
实现完全相同，但这一次，生成的是索引，而不是值。

-------------------

## entries

<font color=#FFF5DF5>entries </font>方法返回生成键值对的生成器。

```js
const entriesGenerator = entries([1, 2, 3, 4, 5]);

entriesGenerator.next(); // { value: [0, 1], done: false }}
```

**实现**

```js
function entries(array) {
 const { length } = array;

 function* createGenerator() {
   for (let index = 0; index < length; index += 1) {
     const value = array[index];
     yield [index, value];
   }
 }

 return createGenerator();
}
```
同样的实现，但现在咱们将索引和值结合起来，并在数组中生成它们。

