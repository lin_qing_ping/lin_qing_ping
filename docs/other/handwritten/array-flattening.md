# 手写数组扁平化

数组扁平化是指将一个多维数组变为一个一维数组

```js
const arr = [1, [2, [3, [4, 5]]], 6];
// => [1, 2, 3, 4, 5, 6]
```

**方法一：使用flat()**
```js
const res1 = arr.flat(Infinity);
```

**方法二：利用正则**
```js
const res2 = JSON.stringify(arr).replace(/\[|\]/g, '').split(',');
```
但数据类型都会变为字符串

**方法三：正则改良版本**
```js
const res3 = JSON.parse('[' + JSON.stringify(arr).replace(/\[|\]/g, '') + ']');
```

**方法四：使用reduce**
```js
const flatten = arr => {
  return arr.reduce((pre, cur) => {
    return pre.concat(Array.isArray(cur) ? flatten(cur) : cur);
  }, [])
}
const res4 = flatten(arr);
```

**方法五：函数递归**
```js
const res5 = [];
const fn = arr => {
  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      fn(arr[i]);
    } else {
      res5.push(arr[i]);
    }
  }
}
fn(arr);
```