# 手写类数组转化为数组

类数组是具有length属性，但不具有数组原型上的方法。常见的类数组有arguments、DOM操作方法返回的结果。

**方法一：Array.from**
```js
Array.from(document.querySelectorAll('div'))
```

**方法二：Array.prototype.slice.call()**
```js
Array.prototype.slice.call(document.querySelectorAll('div'))
```

**方法三：扩展运算符**
```js
[...document.querySelectorAll('div')]
```
当然也可以用include、filter，思路大同小异。

**方法四：利用concat**
```js
Array.prototype.concat.apply([], document.querySelectorAll('div'));
```

**方法五：利用filter**
```js
const unique4 = arr => {
  return arr.filter((item, index) => {
    return arr.indexOf(item) === index;
  });
}
```

**方法六：利用Map**
```js
const unique5 = arr => {
  const map = new Map();
  const res = [];
  for (let i = 0; i < arr.length; i++) {
    if (!map.has(arr[i])) {
      map.set(arr[i], true)
      res.push(arr[i]);
    }
  }
  return res;
}
```