# 手写debounce（防抖）

触发高频时间后n秒内函数只会执行一次,如果n秒内高频时间再次触发,则重新计算时间。

```js
const debounce = (fn, time) => {
  let timeout = null;
  return function() {
    clearTimeout(timeout)
    timeout = setTimeout(() => {
      fn.apply(this, arguments);
    }, time);
  }
};
```

防抖就是在出发后的一段时间内执行一次，例如：在进行搜索的时候，当用户停止输入后调用方法，节约请求资源