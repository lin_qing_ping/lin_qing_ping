# 手写throttle（节流）

高频时间触发,但n秒内只会执行一次,所以节流会稀释函数的执行频率。

```js
const throttle = (fn, time) => {
  let flag = true;
  return function() {
    if (!flag) return;
    flag = false;
    setTimeout(() => {
      fn.apply(this, arguments);
      flag = true;
    }, time);
  }
}
```

节流常应用于鼠标不断点击触发、监听滚动事件。
节流就是在频繁触发某个事件的情况下，每隔一段时间请求一次，类似打游戏的时候长按某个按键，动作是有规律的在间隔时间触发一次